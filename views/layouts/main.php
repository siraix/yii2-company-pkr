<?php

/* @var $this \yii\web\View */

/* @var $content string */

use yii\bootstrap4\Alert;
use yii\helpers\Html;
use yii\bootstrap4\Nav;
use yii\bootstrap4\NavBar;
use app\assets\AppAsset;
use yii\helpers\Url;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css">
    <?php $this->registerLinkTag(['rel' => 'icon', 'type' => 'image/png', 'href' => Url::to(['images/favicon.png'])]); ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-white border-bottom shadow-sm">
    <?php
    NavBar::begin([
        'brandLabel' => Yii::$app->name,
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'tag' => 'h5',
            'class' => 'my-0 mr-md-auto font-weight-normal',
        ],
    ]);
    NavBar::end();
    echo Nav::widget([
        'options' => ['class' => 'my-2 my-md-0 mr-md-3'],
        'items' => [
            ['label' => 'Главная', 'class' => 'p-2 text-dark', 'url' => ['/site/index']],
            ['label' => 'Компании', 'url' => ['/company/index']],
        ]]);
    ?>
    <?=
    Yii::$app->user->isGuest ? Html::a('Войти', ['site/login'], ['class' => 'btn btn-outline-primary'])
        : (Html::beginForm(['/site/logout'], 'post')
        . Html::submitButton(
            'Выйти (' . Yii::$app->user->identity->username . ')',
            ['class' => 'btn btn-outline-primary logout'])
        . Html::endForm());
    ?>
</div>
<div id="main" class="container pt-2 pb-5">
    <?php
    foreach (Yii::$app->session->getAllFlashes() as $key => $message) {
        if (Yii::$app->getSession()->hasFlash($key)) {
            echo Alert::widget();
        }
    }
    ?>
    <?= $content ?>

</div>

<footer class="fixed-bottom text-muted text-center text-small bg-light">
    <p class="mb-1">© 2019 Aleksey Dubov</p>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
