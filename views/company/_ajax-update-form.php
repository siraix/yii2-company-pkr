<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\company\CompanyRecord */
/* @var $form yii\widgets\ActiveForm */
?>
    <div class="company-record-form">
        <?php
        $form = ActiveForm::begin([
            'options' => [
                'id' => 'update-ajax-form',
                'data-pjax' => true,
                'data-record-id' => $model->id
            ],
        ]);
        ?>

        <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'inn')->textInput(['placeholder' => '12 цифр номера ИНН', 'maxlength' => true]) ?>

        <?= $form->field($model, 'company_CEO')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>

        <div class="form-group">
            <?= Html::submitButton('Сохранить', ['id' => 'btn-form-send', 'class' => 'btn btn-success']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>


<?php

