<?php

use yii\helpers\Html;
use yii\bootstrap4\Breadcrumbs;
use yii\web\YiiAsset;
use yii\widgets\DetailView;
use yii\widgets\Pjax;
use yii\web\JqueryAsset;

/* @var $this yii\web\View */
/* @var $model app\models\company\CompanyRecord */


$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Компании', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
YiiAsset::register($this);
$this->registerJsFile('@web/js/company/ajax.js', ['depends' => [JqueryAsset::className()]]);

echo Breadcrumbs::widget(['links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],]);
?>

<?php Pjax::begin(['id' => 'company-view']); ?>
    <div class="row">
        <div id="record-view" class="col-6">

            <h1><?= Html::encode($this->title) ?></h1>
            <?php if (Yii::$app->user->identity->isAdmin()) : ?>
                <p>
                    <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
                        'class' => 'btn btn-danger',
                        'data' => [
                            'confirm' => 'Вы точно хотите удалить эту компанию?',
                            'method' => 'post',
                        ],
                    ]) ?>
                </p>
            <?php endif; ?>

            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'id',
                    'title',
                    'inn',
                    'company_CEO',
                    'address',
                ],
            ]) ?>

        </div>

        <div id="record-edit" class="col-6">

            <?php
            if (Yii::$app->user->identity->isAdmin()) {
                echo $this->render('_ajax-update-form', [
                    'model' => $model
                ]);
            }
            Pjax::end();
            ?>
        </div>
    </div>

<?php
