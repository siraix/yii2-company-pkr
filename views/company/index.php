<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\bootstrap4\LinkPager;

/* @var $this yii\web\View */
/* @var $searchModel app\models\company\CompanyRecordSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Список компаний';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-12 company-record-index">

        <h1><?= Html::encode($this->title) ?></h1>

        <?php if (Yii::$app->user->identity->isAdmin()) : ?>
        <p>
            <?= Html::a('Добавить компанию', ['create'], ['class' => 'btn btn-success']) ?>
        </p>
        <?php endif; ?>

        <?php Pjax::begin(['id' => 'company-list']); ?>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'pager' => [
                    'class'=> LinkPager::className(),
            ],
            'columns' => [
                'id',
                'title',
                'inn',
                'company_CEO',
                'address',

                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{view}',
                    'buttons' => [
                        'view' => function ($url) {
                            return Html::a('<span class="badge badge-secondary"> <i class="far fa-eye"></i></span>', $url);
                        },
                    ],
                ],
            ],
        ]); ?>

        <?php Pjax::end(); ?>

    </div>
</div>
