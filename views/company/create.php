<?php

use yii\bootstrap4\Alert;
use yii\helpers\Html;
use yii\web\JqueryAsset;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model app\models\company\CompanyRecord */

$this->title = 'Добавить компанию';
$this->params['breadcrumbs'][] = ['label' => 'Company Records', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$this->registerJsFile('@web/js/company/ajax.js', ['depends' => [JqueryAsset::className()]]);

?>

<div class="row">
    <div class="col-12 company-record-create">
        <h1><?= Html::encode($this->title) ?></h1>

        <?php Pjax::begin(['id' => 'company-view']); ?>

        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>
        <?php Pjax::end(); ?>

    </div>
</div>
