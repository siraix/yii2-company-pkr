<?php

/* @var $this yii\web\View */

$this->title = 'Yii2-Компании';
?>
<div class="row">
    <div class="col-12 site-index">

        <div class="jumbotron">
            <h1>Yii2 - компании!</h1>

            <p class="lead">Проект на базе Yii2, MySql и элементами Js</p>

            <p><a class="btn btn-lg btn-success" href="/company/index">Изучить компании</a></p>
        </div>

        <div class="body-content">

            <div class="row">
                <div class="col-lg-12">
                    <h2>Реализовано</h2>

                    <ul class="list-unstyled">
                        <li>Авторизация через базу данных</li>
                        <li>RBAC</li>
                        <li>Просмотр компаний</li>
                        <li>Добавление и редактирование компаний администратором</li>
                        <li>Всплывающее уведомление при отправке ajax-а</li>
                        <li>Pjax перезагрузка страницы</li>
                    </ul>
                </div>
            </div>

        </div>
    </div>
</div>