$(document).on('click', '#btn-form-send', function () {

    // Обновление данных о компании
    $('#update-ajax-form').on('beforeSubmit', function () {
        let recordId = $("#update-ajax-form").data('record-id');
        let data = $(this).serialize();

        $.ajax({
            url: '/ajax/update-company?id=' + recordId,
            type: 'post',
            data: data,
            success: (function (data) {
                if (data.success === true) {
                    showAlert(data.data, 'success');
                } else {
                    showAlert(data.data, 'warning');
                }
            }),
            error: (function () {
                showAlert('Произошла ошибка!', 'danger');
            })
        });
    });

    // Создание компании
    $('#create-ajax-form').on('beforeSubmit', function () {
        let data = $(this).serialize();

        $.ajax({
            url: '/ajax/create-company',
            type: 'post',
            data: data,
            success: (function (data) {
                if (data.success === true) {
                    showAlert(data.data, 'success');
                } else {
                    showAlert(data.data, 'warning');
                }
            }),
            error: (function () {
                showAlert('Произошла ошибка!', 'danger');
            })
        });
    });

    /**
     * @param data
     * @param status
     */
    function showAlert(data, status) {
        $('#main').prepend('<div id="alert" class="alert alert-' + status + ' alert-dismissible fade show" role="alert">\n' +
            data + '\n' +
            '  <button type="button" class="close" data-dismiss="alert" aria-label="Закрыть">\n' +
            '    <span aria-hidden="true">&times;</span>\n' +
            '  </button>\n' +
            '</div>');
        $('#alert').delay(3000).fadeOut(800)
    }
});