<?php

namespace app\controllers;

use app\models\company\CompanyRecord;
use yii\helpers\VarDumper;
use yii\web\Controller;
use Yii;

/**
 * Class AjaxController
 * @package app\controllers
 */
class AjaxController extends Controller
{
    /**
     * @param $action
     * @return bool
     * @throws \yii\web\BadRequestHttpException
     */
    public function beforeAction($action)
    {
        if(!Yii::$app->request->isAjax) {
            throw new \Exception('Неверный тип запроса. Разрешенный тип: Ajax');
        }
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return parent::beforeAction($action);
    }

    /**
     * @param $id
     * @return array
     */
    public function actionUpdateCompany($id)
    {
        $request = Yii::$app->getRequest();
        $model = CompanyRecord::findOne($id);

        if ($model->load($request->post()) && $model->save()) {
            return [
                'success' => true,
                'data' => 'Данные успешно обновлены',
            ];
        }
        return [
            'success' => false,
            'data' => 'Не удалось обновить данные: ' . VarDumper::dumpAsString($model->getErrors()),
        ];
    }

    /**
     * @return array
     */
    public function actionCreateCompany()
    {
        $request = Yii::$app->getRequest();
        $model = new CompanyRecord();

        if ($model->load($request->post()) && $model->save()) {
            return [
                'success' => true,
                'data' => 'Данные успешно обновлены',
            ];
        }
        return [
            'success' => false,
            'data' => 'Не удалось создать компанию. Ошибка: ' . VarDumper::dumpAsString($model->getErrors()),
        ];
    }
}