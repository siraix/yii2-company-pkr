<?php

use yii\db\Migration;

/**
 * Class m190725_174832_create_company_table
 */
class m190725_174832_create_company_table extends Migration
{
    /**
     * @return bool|void
     */
    public function safeUp()
    {
        $this->createTable('{{%company}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string(255)->notNull(),
            'inn' => $this->string(32)->notNull(),
            'company_CEO' => $this->string(255)->notNull(),
            'address' => $this->string(255)->null(),
        ]);
    }

    /**
     * @return bool|void
     */
    public function safeDown()
    {
        $this->dropTable('{{%company}}');
    }
}
