<?php

use yii\db\Migration;
use app\models\User;

/**
 * Class m190729_061843_rbac_init_up
 */
class m190729_061843_rbac_init_up extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

        $user = new User();
        $user->username = 'admin';
        $user->setPassword('admin');
        $user->generateAuthKey();
        $user->save();

        $auth = Yii::$app->authManager;
        $admin = $auth->createRole('admin');
        $admin->description = "Администратор";
        $auth->add($admin);
        $auth->assign($admin, $user->id);


        $user = new User();
        $user->username = 'guest';
        $user->setPassword('guest');
        $user->generateAuthKey();
        $user->save();

        $guest = $auth->createRole('guest');
        $guest->description = "Гость";
        $auth->add($guest);
        $auth->assign($guest, $user->id);
    }

    /**
     * @return bool|void
     * @throws Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function safeDown()
    {
        $auth = Yii::$app->authManager;

        $auth->removeAll();
        $users = User::find()->where(['username' => ['admin', 'guest']])->all();
        foreach ($users as $user) {
            $user->delete();
        }
    }

}
