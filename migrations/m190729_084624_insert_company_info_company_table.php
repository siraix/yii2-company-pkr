<?php

use yii\db\Migration;

/**
 * Class m190729_084624_insert_company_info_company_table
 */
class m190729_084624_insert_company_info_company_table extends Migration
{
    /**
     * @return bool|void
     * @throws Exception
     */
    public function safeUp()
    {
        $faker = Faker\Factory::create();

        for ($i = 0; $i < 60; $i++) {
            $this->insert('{{%company}}', [
                'id' => $i + 1,
                'title' => $faker->words(rand('1', '3'), true),
                'inn' => random_int('100000000000', '999999999999'),
                'company_CEO' => $faker->name(),
                'address' => $faker->address
            ]);
        }
    }

    /**
     * @return bool|void
     */
    public function safeDown()
    {
        $this->delete('{{%company}}', ['between', 'id', '1', '60']);
    }

}
