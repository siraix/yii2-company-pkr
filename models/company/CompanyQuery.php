<?php

namespace app\models\company;

use yii\db\ActiveQuery;

/**
 * This is the ActiveQuery class for [[CompanyRecord]].
 *
 * @see CompanyRecord
 */
class CompanyQuery extends ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return CompanyRecord[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return CompanyRecord|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
