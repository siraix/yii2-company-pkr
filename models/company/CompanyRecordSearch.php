<?php

namespace app\models\company;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\company\CompanyRecord;

/**
 * CompanyRecordSearch represents the model behind the search form of `app\models\company\CompanyRecord`.
 */
class CompanyRecordSearch extends CompanyRecord
{
    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['title', 'inn', 'company_CEO', 'address'], 'safe'],
        ];
    }

    /**
     * @return array
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CompanyRecord::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'inn', $this->inn])
            ->andFilterWhere(['like', 'company_CEO', $this->company_CEO])
            ->andFilterWhere(['like', 'address', $this->address]);

        return $dataProvider;
    }
}
