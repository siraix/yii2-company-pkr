<?php

namespace app\models\company;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "company".
 *
 * @property int $id
 * @property string $title
 * @property string $inn
 * @property string $company_CEO
 * @property string $address
 */
class CompanyRecord extends ActiveRecord
{
    /**
     * @return string
     */
    public static function tableName()
    {
        return 'company';
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['title', 'inn', 'company_CEO'], 'required'],
            [['title', 'company_CEO', 'address'], 'string', 'max' => 255],
            ['inn', 'is12NumbersOnly'],
        ];
    }

    /**
     * Валидация на корректность ввода ИНН
     * @param $attribute
     */
    public function is12NumbersOnly($attribute)
    {
        if (!preg_match('/^[0-9]{12}$/', $this->$attribute)) {
            $this->addError($attribute, 'должен быть числом из 12 цифр');
        }
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Название',
            'inn' => 'ИНН',
            'company_CEO' => 'Генеральный директор',
            'address' => 'Адрес',
        ];
    }

    /**
     * {@inheritdoc}
     * @return CompanyQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new CompanyQuery(get_called_class());
    }
}
